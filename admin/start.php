<?php
/**
 * Created by PhpStorm.
 * User: andre
 * Date: 05.05.2017
 * Time: 14:02
 */
session_start();
require_once("../functions/db_connect.php");
require_once("../functions/users.php");
if (!(checkUser($_SESSION["username"], md5($_SESSION["password"])) && isAdmin($_SESSION["username"]))) {
    header("Location: /All_Notebooks/admin/auth.php");
    exit;
}
?>