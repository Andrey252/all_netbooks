<?php
if (isset($_POST['reg'])) {
    $product = htmlspecialchars($_POST['product']);
    $name = htmlspecialchars($_POST['name']);
    $slide = htmlspecialchars($_POST['slide']);
    $new = htmlspecialchars($_POST['new']);
    $price = htmlspecialchars($_POST['price']);
    $display = htmlspecialchars($_POST['display']);
    $display_size = htmlspecialchars($_POST['display_size']);
    $CPU = htmlspecialchars($_POST['CPU']);
    $frequency = htmlspecialchars($_POST['frequency']);
    $RAM = htmlspecialchars($_POST['RAM']);
    $memory = htmlspecialchars($_POST['memory']);
    $GPU = htmlspecialchars($_POST['GPU']);
    $other = htmlspecialchars($_POST['other']);

    if ($new != null && $new != "0") $new = 1;
    if ($slide != null && $slide != "0") $slide = 1;

    session_start();

    $uploaddir = '../css/images/products/';
    $uploadfile = $uploaddir . basename($_FILES['image']['name']);
    $image = basename($_FILES['image']['name']);
    $image = explode(".", $image);

    if (move_uploaded_file($_FILES['image']['tmp_name'], $uploadfile)) {
        echo "Файл корректен и был успешно загружен.\n";
    } else {
        echo "Возможная атака с помощью файловой загрузки!\n";
    }

    addProduct($link, $product, $name, $image[0], $slide, $new, $price, $display, $display_size, $CPU, $frequency, $RAM, $memory, $GPU, $other);
    $_SESSION["reg_success"] = 1;

}
?>