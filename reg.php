<?php
session_start();
require_once("functions/users.php");
$value_page = 3;
include("functions/db_connect.php");
if (isset($_POST['reg'])) {
    $username = htmlspecialchars($_POST['username']);
    $email = htmlspecialchars($_POST['email']);
    $password = htmlspecialchars($_POST['password']);
    $bad = false;
    session_start();
    unset($_SESSION['error_username']);
    unset($_SESSION['error_email']);
    unset($_SESSION['error_password']);
    unset($_SESSION["reg_success"]);

    if (strlen($username) < 3 || strlen($username) > 20) {
        $_SESSION['error_username'] = 1;
        $bad = true;
    }
    if (strlen($email) < 3 || strlen($email) > 32) {
        $_SESSION['error_email'] = 1;
        $bad = true;
    }
    if (strlen($password) < 6 || strlen($password) > 32) {
        $_SESSION['error_password'] = 1;
        $bad = true;

    }
    if (!$bad) {
        regUser($username, $email, $password);
        $_SESSION["reg_success"] = 1;
        header("Location: index.php");
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Регистрация</title>
    <link rel="stylesheet" href="css/style.css" type="text/css">
</head>
<body>
<div class="shell">
    <?php
    include("application/header.php");
    if ($_SESSION["error_username"] == 1) echo '<p style="text-align: center;  margin-top: 30px;"><span style="color: red">Некорректное имя пользователя!</span></p>';
    if ($_SESSION["error_email"] == 1) echo '<p style="text-align: center;  margin-top: 30px;"><span style="color: red">Некорректный E-mail!</span></p>';
    if ($_SESSION["error_password"] == 1) echo '<p style="text-align: center;  margin-top: 30px;"><span style="color: red">Некорректный пароль!</span></p>';
    echo '
        <div id="main">
            <div class="form">
                <form id="login" style="height: 270px; " name="auth" action="" method="post">
                    <h1 class="h1">Регистрация</h1>
                    <fieldset id="inputs">
                        <input id="username" name="username" type="text" placeholder="Имя пользователя" autofocus required>
                        <input id="username" name="email" type="text" placeholder="E-mail" autofocus required>
                        <input id="password" name="password" type="password" placeholder="Пароль" required>
                    </fieldset>
                    <fieldset id="actions">
                        <input type="submit" name="reg" id="submit" style="width: 200px" value="ЗАРЕГИСТРИРОВАТЬСЯ">
                    </fieldset>
                </form>
            </div>
        </div>
        ';
    include("application/footer.php");
    ?>
</div>
</body>
</html>
